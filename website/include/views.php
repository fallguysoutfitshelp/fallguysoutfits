<?php
//Connection to database
$host = 'localhost';
$user = 'root';
$password = '';
$database = 'data';

/*
Mit dem Datenbank-Server verbinden, Datenbank wählen und Zeichensatz definieren
- mit Fehlerbehandlung

01.09.2020, Tobias Locher
*/

// Verbindung zur Datenbank aufbauen
try {
    $dsn = 'mysql:host=' . $host . ';dbname=' . $database;
    $db = new PDO($dsn, $user, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
}
// Fehler-Behandlung
catch (PDOException $e) {
    // Fehlermeldung ohne Details, wird auch im produktiven Web gezeigt
    echo '<p>Connection lost!';

    // Detaillierte Fehlermeldung, wird nur auf dem Testserver angezeigt (da, wo display_errors auf on gesetzt ist)
    if (ini_get('display_errors')) {
        echo '<br>' . $e->getMessage();
    }

    // Ausführung des Scripts beenden
    exit;
}

//Variablen werden gesetzt
$ipNow = $_SERVER['REMOTE_ADDR'];
$ipReplacedToCheck = str_replace('.', "", $ipNow);
$query = $db->query("SELECT * FROM views");

$ip = array();

//IP adressen werden in array als integer gespeichert
foreach ($query as $row) {
    $ipReplaced = str_replace('.', "", $row['ip']);
    array_push($ip, intval($ipReplaced));
}
//Es wird geschaut, ob die IP schon gespeichert ist
if (in_array($ipReplacedToCheck, $ip)) {
    //Die ID der IP wird gesucht
    $query = $db->query("SELECT * FROM views WHERE ip=\"$ipNow\"");

    $views = array();
    $id = array();
    //Die ID und die views werden ausgeschrieben und 1 View hinzugefügt
    foreach ($query as $row) {
        $views[0] = $row['views'] + 1;
        $id[0] = intval($row['id']);
    }

    //Arrays werden als int gespeichert
    $views = implode(",", $views);
    $id = implode(",", $id);

    //Datenbank wird aktualisiert
    $statement = $db->query("UPDATE views SET views = $views WHERE id=$id");


    //Wenn die IP die Webseite noch nie besucht hat
} else {
    //IP wird gespeichert
    $query = $db->query("INSERT INTO views
    VALUES(NULL, 1, \"$ipNow\")");
}