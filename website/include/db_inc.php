<?php
$host = 'localhost';
$user = 'root';
$password = '';
$database = 'outfits';
$tableUpper = "upper";
$tableLower = "lower";
$tablePattern = "pattern";
$tableColor = "color";
$tableShopextras = "shopextras";
$tableShopRating = "shopRating";
$tableEmotes = "emotes";

define('DEBUG', false);

error_reporting(E_ALL);
ini_set('display_errors', DEBUG ? 'On' : 'Off');