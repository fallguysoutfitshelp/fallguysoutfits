<section class="footer4 cid-s8Ra2I1LkF" once="footers" id="footer4-14">

    <div class="container">
        <div class="row mbr-white">
            <div class="col-12 col-md-6 col-lg-3">
                <div class="media-wrap">
                    <a href="https://fallguys.com">
                        <img src="assets/images/fallguys-506x463.png" alt="Mobirise">
                    </a>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-3">
                <h5 class="mbr-section-subtitle mbr-fonts-style mb-2 display-7">
                    <strong>The Developers</strong></h5>
                <p class="mbr-text mbr-fonts-style mb-4 display-4">We are two passionate Fall Guys players, who immediately realized our need for a skin website.&nbsp;</p>
                <h5 class="mbr-section-subtitle mbr-fonts-style mb-3 display-7">
                    <strong>Follow Us</strong>
                </h5>
                <div class="social-row display-7">
                    <div class="soc-item">
                        <a href="https://www.instagram.com/fallguys.outfits/" target="_blank">
                            <span class="mbr-iconfont socicon-instagram socicon"></span>
                        </a>
                    </div>
                    <!-- <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top"> -->
                    <input type="hidden" name="cmd" value="_donations" />
                    <!-- <input type="hidden" name="business" value="fallguysoutfitshelp@gmail.com" /> -->
                    <input type="hidden" name="item_name" value="Donations to cover our web hosting costs" />
                    <input type="hidden" name="currency_code" value="USD" />
                    <input type="image" id="donateButton" src="https://www.paypalobjects.com/en_US/CH/i/btn/btn_donateCC_LG.gif" borde="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" />
                    <img alt="" borde="0" src="https://www.paypal.com/en_CH/i/scr/pixel.gif" width="1" height="1" />
                    <!--  </form> -->
                </div>
                <p class="mbr-text mbr-fonts-style mb-4 display-4">We do not earn money with this website. Donations are needed to cover part of the web hosting costs.&nbsp;</p>
            </div>
            <div class="col-12 col-md-6 col-lg-3">
                <h5 class="mbr-section-subtitle mbr-fonts-style mb-2 display-7"><strong>Help us!</strong></h5>
                <ul class="list mbr-fonts-style display-4">
                    <li class="mbr-text item-wrap">Help us, make this site better:</li>
                    <li class="mbr-text item-wrap"><br></li>
                    <li class="mbr-text item-wrap"><a href="mailto:fallguysoutfitshelp@gmail.com" class="text-info">fallguysoutfitshelp@gmail.com</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-6 col-lg-3">
                <h5 class="mbr-section-subtitle mbr-fonts-style mb-2 display-7"><strong>About</strong></h5>
                <ul class="list mbr-fonts-style display-4">We are not affiliated with Mediatonic, our content is not official and is not supported by Mediatonic in any way.</ul>
            </div>
        </div>
    </div>
</section>
<script>
    document.getElementById("donateButton").onclick = function() {
        alert("Comming soon");
    }
</script>