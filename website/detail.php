<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <link rel="shortcut icon" href="assets/images/logo-121x126.png" type="image/x-icon">
    <meta name="description" content="">

    <?php
    require_once("include/views.php");
    require_once("include/db_inc.php");
    require_once("include/connect.php");


    $queryDetail = "";
    $table1 = "";
    $table2 = "";
    $table3 = "";

    $type = htmlspecialchars($_GET['type']);
    $idGot = htmlspecialchars($_GET['id']);

    if ($type == "upper") {
        $queryDetail = "SELECT * FROM $tableUpper WHERE id = " . $idGot;
        $table1 = "lower";
        $table2 = "color";
        $table3 = "pattern";
    } elseif ($type == "lower") {
        $queryDetail = "SELECT * FROM $tableLower WHERE id = " . $idGot;
        $table1 = "upper";
        $table2 = "color";
        $table3 = "pattern";
    } elseif ($type == "pattern") {
        $queryDetail = "SELECT * FROM $tablePattern WHERE id = " . $idGot;
        $table1 = "upper";
        $table2 = "lower";
        $table3 = "color";
    } elseif ($type == "color") {
        $queryDetail = "SELECT * FROM $tableColor WHERE id = " . $idGot;
        $table1 = "upper";
        $table2 = "lower";
        $table3 = "pattern";
    } elseif ($type == "emote") {
        $queryDetail = "SELECT * FROM $tableEmotes WHERE id = " . $idGot;
        $table1 = "upper";
        $table2 = "lower";
        $table3 = "pattern";
    }

    $queryRelated1 = "SELECT * FROM $table1";
    $queryRelated2 = "SELECT * FROM $table2";
    $queryRelated3 = "SELECT * FROM $table3";

    $name = "";

    $result = $db->query($queryDetail);
    ?>
    <title>Fall Guys Skins -
        <?php
        foreach ($result as $row) {
            echo $row['name'];
        }
        ?> [FGO]</title>

    <?php

    use function PHPSTORM_META\type;

    require_once("blocks/stylesheets.html");
    ?>

</head>

<body>


    <section class="menu cid-s8RbCuPujQ" once="menu" id="menu1-18">


    </section>
    <section class="menu cid-s8Ra0fAvKt" once="menu" id="menu1-w">

        <?php require_once("blocks/nav.php"); ?>

    </section>

    <div>
        <?php require_once("blocks/alert.php"); ?>
    </div>

    <section class="image1 cid-s8RbYQhvMS" id="image1-1b">
        <?php
        $queryDetail = "";
        $table1 = "";
        $table2 = "";
        $table3 = "";

        if ($type == "upper") {
            $queryDetail = "SELECT * FROM $tableUpper WHERE id = " . $idGot;
            $table1 = "lower";
            $table2 = "color";
            $table3 = "pattern";
        } elseif ($type == "lower") {
            $queryDetail = "SELECT * FROM $tableLower WHERE id = " . $idGot;
            $table1 = "upper";
            $table2 = "color";
            $table3 = "pattern";
        } elseif ($type == "pattern") {
            $queryDetail = "SELECT * FROM $tablePattern WHERE id = " . $idGot;
            $table1 = "upper";
            $table2 = "lower";
            $table3 = "color";
        } elseif ($type == "color") {
            $queryDetail = "SELECT * FROM $tableColor WHERE id = " . $idGot;
            $table1 = "upper";
            $table2 = "lower";
            $table3 = "pattern";
        } elseif ($type == "emote") {
            $queryDetail = "SELECT * FROM $tableEmotes WHERE id = " . $idGot;
            $table1 = "upper";
            $table2 = "lower";
            $table3 = "pattern";
        }

        $queryRelated1 = "SELECT * FROM $table1";
        $queryRelated2 = "SELECT * FROM $table2";
        $queryRelated3 = "SELECT * FROM $table3";

        $name = "";

        $result = $db->query($queryDetail);
        foreach ($result as $row) {
            $sold = "";
            $psnOnly = false;
            $steamOnly = false;
            $currency = "";
            $rarity = "";
            $path = "img/" . $row['path'];

            $name = $row['name'];

            //Farben
            $farbcodeCommon = "#06f4ec";
            $farbcodeUncommon = "#15b1ec";
            $farbcodeRare = "#3cf171";
            $farbcodeEpic = "#9f40b2";
            $farbcodeLegendary = "#fb911a";
            $entsprechendeFarbe = "";

            // Abfrage nach Verkaufstyp und Währund
            if ($row['sold'] == 1) {
                $sold = "Available in Shop";
                if ($row['currency'] == 1) {
                    $currency = "Kuddos";
                } elseif ($row['currency'] == 2) {
                    $currency = "Crowns";
                }
            } elseif ($row['sold'] == 2) {
                $sold = "Available in Battle Pass";
            } elseif ($row['sold'] == 3) {
                $sold = "Available in a DLC";
            } elseif ($row['sold'] == 4) {
                $sold = "Available in another way";
            }

            // Abfrage ob PSOnly/Steamonly
            if ($row['psnOnly'] == 1) {
                $psnOnly = true;
            } elseif ($row['steamOnly'] == 1) {
                $steamOnly = true;
            }

            // Abfrage Seltenheit
            if ($row['rarity'] == 1) {
                $rarity = "Common";
                $entsprechendeFarbe = $farbcodeCommon;
            } elseif ($row['rarity'] == 2) {
                $rarity = "Uncommon";
                $entsprechendeFarbe = $farbcodeUncommon;
            } elseif ($row['rarity'] == 3) {
                $rarity = "Rare";
                $entsprechendeFarbe = $farbcodeRare;
            } elseif ($row['rarity'] == 4) {
                $rarity = "Epic";
                $entsprechendeFarbe = $farbcodeEpic;
            } elseif ($row['rarity'] == 5) {
                $rarity = "Legendary";
                $entsprechendeFarbe = $farbcodeLegendary;
            } ?>


            <div class="container fadeInUp">
                <div class="row align-items-center">
                    <div class="col-12 col-lg-6">
                        <div class="image-wrapper">
                            <img src="<?php echo $path ?>" alt="Picture of Outfit">
                        </div>
                    </div>
                    <div class="col-12 col-lg">
                        <div class="text-wrapper">
                            <h3 class="mbr-section-title mbr-fonts-style mb-3 display-5">
                                <strong> <?php echo ($row['name'] . ' - ' . ucfirst($_GET['type'])) ?></strong></h3>
                            <p class="mbr-text mbr-fonts-style mt-3 display-7" style='<?php echo ("color: $entsprechendeFarbe") ?>'><?php echo ($rarity) ?></p>
                            <p class="mbr-text mbr-fonts-style mt-3 display-7"><?php echo ("Season: " . $row['season']) ?></p>

                            <p class="mbr-text mbr-fonts-style mt-3 display-7"><?php echo ($sold) ?></p>
                            <?php if ($row['sold'] == 2) { ?>
                                <p class="mbr-text mbr-fonts-style mt-3 display-7"><?php echo ("Battle Pass Level:" . $row['battlePassLvl']) ?></p>
                            <?php } ?>
                            <?php if ($row['sold'] == 3) { ?>
                                <p class="mbr-text mbr-fonts-style mt-3 display-7"><?php echo ("DLC: " . $row['dlc']) ?></p>
                            <?php } ?>
                            <?php if ($row['sold'] == 4) { ?>
                                <p class="mbr-text mbr-fonts-style mt-3 display-7"><?php echo ($row['dlc']) ?></p>
                            <?php } ?>
                            <?php if ($psnOnly == true) { ?>
                                <p class="mbr-text mbr-fonts-style mt-3 display-7"><?php echo ("PS4 only") ?></p>
                            <?php } ?>
                            <?php if ($steamOnly == true) { ?>
                                <p class="mbr-text mbr-fonts-style mt-3 display-7"><?php echo ("Steam only") ?></p>
                            <?php } ?>
                            <?php if ($row['sold'] == 1) { ?>
                                <p class="mbr-text mbr-fonts-style mt-3 display-7"><?php echo ($row['price'] . " " . $currency) ?></p>
                            <?php }
                            ?>
                        </div>
                    </div>
                    <!--
                    <div class="col-12 col-lg">
                        <?php

                        ?>
                        <h2>Skin logo will appear here</h2>
                    </div>
                    -->
                <?php } ?>
                </div>
            </div>
    </section>

    <section class="features2 cid-s8Rc9J5lEi" id="features3-1c">


        <div class="container">
            <div class="mbr-section-head">
                <h4 class="mbr-section-title mbr-fonts-style align-center mb-0 display-2"><strong>Related</strong></h4>

            </div>
            <div class="row mt-4">
                <?php
                foreach ($db->query($queryRelated1) as $row) {
                    if ($name == $row['name']) {
                        require("blocks/skinFields.php");
                    }
                } ?>
                <?php
                foreach ($db->query($queryRelated2) as $row) {
                    if ($name == $row['name']) {
                        require("blocks/skinFields.php");
                    }
                } ?>
                <?php
                foreach ($db->query($queryRelated3) as $row) {
                    if ($name == $row['name']) {
                        require("blocks/skinFields.php");
                    }
                } ?>
            </div>
    </section>

    <?php
    require_once("blocks/footer.php");
    ?>

    <?php
    require_once("blocks/javascript.html");
    ?>



</body>

</html>