<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin - [FGO]</title>
    <link rel="shortcut icon" href="../assets/images/logo-121x126.png" type="image/x-icon">

    <!-- CSS Files -->
    <link rel="stylesheet" href="../assets/tether/tether.min.css">
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="../assets/dropdown/css/style.css">
    <link rel="stylesheet" href="../assets/socicon/css/styles.css">
    <link rel="stylesheet" href="../assets/theme/css/style.css">
    <link rel="preload" as="style" href="../assets/mobirise/css/mbr-additional.css">
    <link rel="stylesheet" href="../assets/mobirise/css/mbr-additional.css" type="text/css">
    <link rel="stylesheet" href="../assets/css/skinFields.css" type="text/css">
    <link rel="stylesheet" href="../assets/css/layouts.css">
    <link rel="stylesheet" href="../assets/css/animation.css">
    <link rel="stylesheet" href="admin.css">
</head>

<body>
    <?php
    require_once("../include/db_inc.php");
    require_once("../include/connect.php");

    $query = $db->query("SELECT * FROM admin");

    foreach ($query as $row) {
        $usernPass[$row['username']] = $row['password'];
    }
    ?>
    <div class="container" id="loginDialogue">
        <div class="row">
            <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                <div class="card card-signin my-5">
                    <div class="card-body">
                        <h5 class="card-title text-center">Sign In</h5>
                        <form class="form-signin" action="<?php $_SERVER['PHP_SELF'] ?>" method="POST">
                            <div class="form-label-group">
                                <input type="text" id="username" class="form-control" placeholder="Username" name="username" required autofocus>
                                <label for="username">Username</label>
                                <?php
                                if (isset($_POST['username']) && $_POST['username'] == "") {
                                    echo "<small style=\"color: red;\">Username is missing";
                                }
                                ?>
                            </div>

                            <div class="form-label-group">
                                <input type="password" id="inputPassword" class="form-control" placeholder="Password" name="password" required>
                                <label for="inputPassword">Password</label>
                                <?php
                                if (isset($_POST['password']) && $_POST['password'] == "") {
                                    echo "<small style=\"color: red;\">Password is missing";
                                }
                                ?>
                            </div>

                            <!--   <div class="custom-control custom-checkbox mb-3">
                                <input type="checkbox" class="custom-control-input" id="customCheck1">
                                <label class="custom-control-label" for="customCheck1">Remember password</label>
                            </div> -->
                            <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit" id="notReady">Sign in</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Scripts -->
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="admin.js"></script>
</body>

</html>