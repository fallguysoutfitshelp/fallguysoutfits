<?php
/*if (isset($_POST['buttonSearch'])) {
    if (isset($_POST['search'])) {
        $searchText = $_POST['search'];
        $searchText = htmlspecialchars($searchText);
        $searchText = strip_tags($searchText);
        $searchText = trim($searchText);
        $searchText = preg_match("/[a-zA-Z]/", $_POST["name"]);

        $querySearch = "SELECT * FROM $tableUpper,$tableLower,$tablePattern,$tableColor WHERE $tableColor.name = :name OR $tableUpper.name = :name OR $tableLower.name = :name OR $tablePattern.name = :name";

        $preparedStatementSearch = $db->prepare($querySearch);
        $preparedStatementSearch->bind_param(':name', $searchText);
        $preparedStatementSearch->execute();
    }
}*/
?>

<!DOCTYPE html>
<html>

<head>
    <!-- Site made with Mobirise Website Builder v5.0.29, https://mobirise.com -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="generator" content="Mobirise v5.0.29, mobirise.com">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <link rel="shortcut icon" href="assets/images/logo-121x126.png" type="image/x-icon">
    <meta name="description" content="">


    <title>Fall Guys Skins - Search [FGO]</title>

    <?php
    require_once("blocks/stylesheets.html");
    ?>

</head>

<body>
    <?php
    require_once("include/views.php");
    require_once("include/db_inc.php");
    require_once("include/connect.php");

    $table = $tableUpper;
    ?>
    <section class="menu cid-s8Ra0fAvKt" once="menu" id="menu1-w">

        <?php require_once("blocks/nav.php"); ?>

    </section>

    <section class="features2 cid-s8Ra0hEIR2" id="features3-x">

        <div>
            <?php require_once("blocks/alert.php"); ?>
        </div>

        <div class="container">
            <div class="mbr-section-head">
                <h4 class="mbr-section-title mbr-fonts-style align-center mb-0 display-2">
                    <strong>Not available yet!</strong></h4>
            </div>
        </div>
        <?php
        while ($row = $statementSearch->fetch()) {
            require("blocks/skinFields.php");
        }
        ?>
    </section>

    <!-- Javascript -->
    <?php
    require_once("blocks/footer.php");
    ?>


    <?php
    require_once("blocks/javascript.html");
    ?>



</body>

</html>