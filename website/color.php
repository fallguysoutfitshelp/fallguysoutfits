<!DOCTYPE html>
<html>

<head>
    <!-- Site made with Mobirise Website Builder v5.0.29, https://mobirise.com -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <link rel="shortcut icon" href="assets/images/logo-121x126.png" type="image/x-icon">
    <meta name="description" content="Collection of all Fall Guys Colors">
    <meta name="keywords" content="Fall Guys Skins, Fall, Guys, Skins, Outfits, Fallguys, Color">
    <meta name="author" content="Giotsche, Tobi">


    <title>Fall Guys Skins - Colour [FGO]</title>

    <?php
    require_once("blocks/stylesheets.html");
    ?>




</head>

<body>

    <section class="menu cid-s8Ra428PEI" once="menu" id="menu1-15">

        <?php
        require_once("include/views.php");
        require_once("include/db_inc.php");
        require_once("include/connect.php");
        require_once("blocks/nav.php");

        $table = $tableColor
        ?>

    </section>

    <section class="features2 cid-s8Ra44djMj" id="features3-16">

        <div>
            <?php require_once("blocks/alert.php"); ?>
        </div>

        <div class="container">
            <div class="mbr-section-head">
                <h4 class="mbr-section-title mbr-fonts-style align-center mb-0 display-2">
                    <strong>Color</strong></h4>

            </div>
            <div class="row mt-4">
                <?php require("blocks/sort.php"); ?>
            </div>
        </div>
    </section>

    <?php
    require_once("blocks/footer.php");
    ?>

    <?php
    require_once("blocks/javascript.html");
    ?>



</body>

</html>