$button = document.getElementById("closeAlert");

$button.onclick = function () {
    $button.parentElement.classList.add("fadeOutUp");
    setTimeout(closeAlert, 1000);
}

function closeAlert() {
    $button.parentElement.style.display = 'none';
}

$(document).ready(function () {
    $("#myModal").modal('show');
});

$('#myModal').on('shown.bs.modal', function () {
    $('#myInput').trigger('focus')
})
