var shop = document.getElementById("titleShop");

if (shop) {
    document.getElementById("shop").classList.add("shopClicked");
}

var lower = document.getElementById("titleLower");

if (lower) {
    document.getElementById("lower").classList.add("lowerClicked");
}

var upper = document.getElementById("titleUpper");

if (upper) {
    document.getElementById("upper").classList.add("upperClicked");
}

var pattern = document.getElementById("titlePattern");

if (pattern) {
    document.getElementById("pattern").classList.add("patternClicked");
}