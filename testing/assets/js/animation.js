$button = document.getElementById("closeAlert");

$button.onclick = function () {
    $button.parentElement.classList.add("fadeOutUp");
    setTimeout(closeAlert, 1000);
}

function closeAlert() {
    $button.parentElement.style.display = 'none';
}