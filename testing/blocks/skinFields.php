<?php
$sold = "";
$psnOnly = false;
$steamOnly = false;
$currency = "";
$rarity = "";
$path = "img/" . $row['path'];
$isVideo = false;

if (!file_exists($path)) {
    $path = "img/nopicture.png";
}

//Farben
$farbcodeCommon = "#06f4ec";
$farbcodeUncommon = "#15b1ec";
$farbcodeRare = "#3cf171";
$farbcodeEpic = "#9f40b2";
$farbcodeLegendary = "#fb911a";
$entsprechendeFarbe = "";

// herausfinden des Types
$remove1 = array(' ');
$skinnameWithoutSpaces = str_replace($remove1, "", $row['name']);
$skinnameInLowerCase = strtolower($skinnameWithoutSpaces);

$remove2 = array('.png', '.gif', "$skinnameInLowerCase");
$type = str_replace($remove2, "", $row['path']);

// Abfrage nach Verkaufstyp und Währund
if ($row['sold'] == 1) {
    $sold = "Available in Shop";
    if ($row['currency'] == 1) {
        $currency = "Kuddos";
    } elseif ($row['currency'] == 2) {
        $currency = "Crowns";
    }
} elseif ($row['sold'] == 2) {
    $sold = "Available in Battle Pass";
} elseif ($row['sold'] == 3) {
    $sold = "Available in a DLC";
} elseif ($row['sold'] == 4) {
    $sold = "Available in another way";
}

// Abfrage ob PSOnly/Steamonly
if ($row['psnOnly'] == 1) {
    $psnOnly = true;
} elseif ($row['steamOnly'] == 1) {
    $steamOnly = true;
}

//Abfrage ob es ein Video ist
if ($row['isVid'] == 1) {
    $isVideo = true;
}

// Abfrage Seltenheit
if ($row['rarity'] == 1) {
    $rarity = "Common";
    $entsprechendeFarbe = $farbcodeCommon;
} elseif ($row['rarity'] == 2) {
    $rarity = "Uncommon";
    $entsprechendeFarbe = $farbcodeUncommon;
} elseif ($row['rarity'] == 3) {
    $rarity = "Rare";
    $entsprechendeFarbe = $farbcodeRare;
} elseif ($row['rarity'] == 4) {
    $rarity = "Epic";
    $entsprechendeFarbe = $farbcodeEpic;
} elseif ($row['rarity'] == 5) {
    $rarity = "Legendary";
    $entsprechendeFarbe = $farbcodeLegendary;
} ?>
<div class="item features-image сol-12 col-md-6 col-lg-4">
    <div class="item-wrapper">
        <div class="item-img">
            <img src="<?php echo $path ?>">
        </div>
        <div class="item-content">
            <h5 class="item-title mbr-fonts-style display-7"><strong><?php echo ($row['name'] . '     -     ' . ucfirst($type)) ?></strong></h5>

            <p class="mbr-text mbr-fonts-style mt-3 display-7" style='<?php echo ("color: $entsprechendeFarbe") ?>'><?php echo ($rarity) ?></p>
            <p class="mbr-text mbr-fonts-style mt-3 display-7"><?php echo ("Season: " . $row['season']) ?></p>
            <p class="mbr-text mbr-fonts-style mt-3 display-7"><?php echo ($sold) ?></p>
            <?php if ($row['sold'] == 2) { ?>
                <p class="mbr-text mbr-fonts-style mt-3 display-7"><?php echo ("Battle Pass Level: " . $row['battlePassLvl']) ?></p>
            <?php } ?>
            <?php if ($row['sold'] == 3) { ?>
                <p class="mbr-text mbr-fonts-style mt-3 display-7"><?php echo ("DLC: " . $row['dlc']) ?></p>
            <?php } ?>
            <?php if ($row['sold'] == 4) { ?>
                <p class="mbr-text mbr-fonts-style mt-3 display-7"><?php echo ($row['dlc']) ?></p>
            <?php } ?>
            <?php if ($psnOnly == true) { ?>
                <p class="mbr-text mbr-fonts-style mt-3 display-7"><?php echo ("PS4 only") ?></p>
            <?php } ?>
            <?php if ($steamOnly == true) { ?>
                <p class="mbr-text mbr-fonts-style mt-3 display-7"><?php echo ("Steam only") ?></p>
            <?php } ?>
            <?php if ($row['sold'] == 1) { ?>
                <p class="mbr-text mbr-fonts-style mt-3 display-7"><?php echo ($row['price'] . " " . $currency) ?></p>
            <?php }
            ?>
        </div>
        <?php $detailLink = "http://fallguysoutfits.com/detail.php?id=" . $row['id'] . "&type=" . $type; ?>
        <div class="mbr-section-btn item-footer mt-2"><a href=<?php echo $detailLink ?> class="btn btn-primary item-btn display-7">More
                &gt;</a></div>
    </div>
</div>