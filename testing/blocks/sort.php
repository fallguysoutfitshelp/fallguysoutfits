<?php
$querySortNot = "SELECT * FROM $table";
$querySortName = "SELECT * FROM $table ORDER BY name";
$querySortRarity = "SELECT * FROM $table ORDER BY rarity DESC";
$querySortSeason = "SELECT * FROM $table ORDER BY season";
$querySortPrice = "SELECT * FROM $table ORDER BY currency, price";
$query = $querySortNot;

if (isset($_POST['buttonNothing'])) {
    $query = $querySortNot;
} elseif (isset($_POST['buttonName'])) {
    $query = $querySortName;
} elseif (isset($_POST['buttonRarity'])) {
    $query = $querySortRarity;
} elseif (isset($_POST['buttonSeason'])) {
    $query = $querySortSeason;
} elseif (isset($_POST['buttonPrice'])) {
    $query = $querySortPrice;
}
?>
<div class="mdc-touch-target-wrapper" style="width:100%">
    <h4>Sort by:</h4>
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" id="formSort">
        <button name="buttonNothing" class="mdc-button mdc-button--touch" href="<?php echo $_SERVER['PHP_SELF']; ?>">
            <div class="mdc-button__ripple"></div>
            <span class="mdc-button__label">Nothing</span>
            <div class="mdc-button__touch"></div>
        </button>

        <button name="buttonName" class="mdc-button mdc-button--touch" href="<?php echo $_SERVER['PHP_SELF']; ?>">
            <div class="mdc-button__ripple"></div>
            <span class="mdc-button__label">Name</span>
            <div class="mdc-button__touch"></div>
        </button>

        <button name="buttonRarity" class="mdc-button mdc-button--touch" href="<?php echo $_SERVER['PHP_SELF']; ?>">
            <div class="mdc-button__ripple"></div>
            <span class="mdc-button__label">Rarity</span>
            <div class="mdc-button__touch"></div>
        </button>

        <button name="buttonSeason" class="mdc-button mdc-button--touch" href="<?php echo $_SERVER['PHP_SELF']; ?>">
            <div class="mdc-button__ripple"></div>
            <span class="mdc-button__label">Season</span>
            <div class="mdc-button__touch"></div>
        </button>

        <button name="buttonPrice" class="mdc-button mdc-button--touch" href="<?php echo $_SERVER['PHP_SELF']; ?>">
            <div class="mdc-button__ripple"></div>
            <span class="mdc-button__label">Price</span>
            <div class="mdc-button__touch"></div>
        </button>
    </form>
</div>