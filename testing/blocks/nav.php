  <nav class="navbar navbar-dropdown navbar-fixed-top navbar-expand-lg">
      <div class="container">
          <div class="navbar-brand">
              <span class="navbar-logo">
                  <a href="http://fallguysoutfits.com">
                      <img src="assets/images/logo-121x126.png" alt="Logo" style="height: 3.8rem;">
                  </a>
              </span>
              <span class="navbar-caption-wrap"><a class="navbar-caption text-black text-primary display-7"
                      href="http://fallguysoutfits.com">Fall Guys Outfits</a></span>
          </div>
          <div class="search-container">
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
              aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
              <div class="hamburger">
                  <span></span>
                  <span></span>
                  <span></span>
                  <span></span>
              </div>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">
                  <li class="nav-item"><a class="nav-link link text-black text-primary display-4"
                          href="http://fallguysoutfits.com">Shop</a></li>
                  <li class="nav-item"><a class="nav-link link text-black text-primary display-4"
                          href="http://fallguysoutfits.com/upper.php">Upper</a></li>
                  <li class="nav-item"><a class="nav-link link text-black text-primary display-4"
                          href="http://fallguysoutfits.com/lower.php">Lower</a></li>
                  <li class="nav-item"><a class="nav-link link text-black text-primary display-4"
                          href="http://fallguysoutfits.com/pattern.php">Pattern</a></li>
                  <!-- <li class="nav-item"><a class="nav-link link text-black text-primary display-4" href="http://fallguysoutfits.com/color.php">Color</a></li>-->
                  <!--    <form action="search.php" method="post"> -->
                  <!-- Search -->
                  <!--   <li><input type="text" id="search" class="form-control" name="search" placeholder="search the website"></li>
                      <li><button type="submit" name="buttonSearch"><i class="fa fa-search"></i></button></li>
                  </form> -->
              </ul>
          </div>
      </div>
  </nav>