<!DOCTYPE html>
<html>

<head>
    <!-- Site made with Mobirise Website Builder v5.0.29, https://mobirise.com -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <link rel="shortcut icon" href="assets/images/logo-121x126.png" type="image/x-icon">
    <meta name="description" content="Collection of all Fall Guys Lower Skins">
    <meta name="keywords" content="Fall Guys Skins, Fall, Guys, Skins, Outfits, Fallguys, Lower">
    <meta name="author" content="Giotsche, Tobi">


    <title>Fall Guys Skins - Lower [FGO]</title>

    <?php
    //Stylesheets einbinden
    require_once("blocks/stylesheets.html");
    ?>

</head>

<body>

    <section class="menu cid-s8Ra1G17fc" once="menu" id="menu1-z">

        <?php
        require_once("include/views.php");
        require_once("include/db_inc.php");
        require_once("include/connect.php");
        require_once("blocks/nav.php");

        $table = $tableLower;

        ?>

    </section>

    <section class="features2 cid-s8Ra1ImmbD" id="features3-10">

        <div class="fadeInDown">
            <?php require_once("blocks/alert.php"); ?>
        </div>

        <div class="container fadeInUp">
            <div class="mbr-section-head">
                <div class="card">
                    <div class="card-header container-fluid">
                        <div class="row">
                            <div class="col-sm lowerTag">
                                <h1 class="p-3">Lower</h1>
                            </div>
                            <div class="col-sm sortPC" style="text-align: right;">
                                <?php require("blocks/sort.php"); ?>
                            </div>
                        </div>
                    </div>
                    <ul class="list-group list-group-flush sortPH">
                        <li class="list-group-item" style="text-align: center;">
                            <?php require("blocks/sort.php"); ?>
                        </li>
                    </ul>
                    <div class="row mt-4">
                        <?php foreach ($db->query($query) as $row) {
                            require("blocks/skinFields.php");
                        } ?>
                    </div>
                </div>
            </div>
    </section>

    <?php
    require_once("blocks/footer.php");
    ?>


    <?php
    require_once("blocks/javascript.html");
    ?>



</body>

</html>