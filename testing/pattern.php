<!DOCTYPE html>
<html>

<head>
    <!-- Site made with Mobirise Website Builder v5.0.29, https://mobirise.com -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <link rel="shortcut icon" href="assets/images/logo-121x126.png" type="image/x-icon">
    <meta name="description" content="Collection of all Fall Guys Patterns">
    <meta name="keywords" content="Fall Guys Skins, Fall, Guys, Skins, Outfits, Fallguys, Pattern">
    <meta name="author" content="Giotsche, Tobi">

    <title>Fall Guys Skins - Pattern [FGO]</title>

    <?php
    require_once("blocks/stylesheets.html");
    ?>

</head>

<body>

    <section class="menu cid-s8Ra2DtZ6l" once="menu" id="menu1-12">

        <?php
        require_once("include/views.php");
        require_once("include/db_inc.php");
        require_once("include/connect.php");
        require_once("blocks/nav.php");

        $table = $tablePattern;
        ?>

    </section>

    <section class="features2 cid-s8Ra2FmVLt" id="features3-13">

        <div class="fadeInDown">
            <?php require_once("blocks/alert.php"); ?>
        </div>

        <div class="container fadeInUp">
            <div class="mbr-section-head">
                <div class="card">
                    <div class="card-header container-fluid">
                        <div class="row">
                            <div class="col-sm patternTag">
                                <h1 class="p-3">Pattern</h1>
                            </div>
                            <div class="col-sm sortPC" style="text-align: right;">
                                <?php require("blocks/sort.php"); ?>
                            </div>
                        </div>
                    </div>
                    <ul class="list-group list-group-flush sortPH">
                        <li class="list-group-item" style="text-align: center;">
                            <?php require("blocks/sort.php"); ?>
                        </li>
                    </ul>
                    <div class="row mt-4">
                        <?php foreach ($db->query($query) as $row) {
                            require("blocks/skinFields.php");
                        } ?>
                    </div>
                </div>
            </div>
    </section>

    <?php
    require_once("blocks/footer.php");
    ?>

    <?php
    require_once("blocks/javascript.html");
    ?>



</body>

</html>