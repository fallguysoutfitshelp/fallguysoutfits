<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="../assets/images/logo-121x126.png" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>[FGO] - Statistics</title>

    <!-- CSS stylesheets -->
    <link rel="stylesheet" href="../assets/css/animation.css">
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="stats.css">
</head>

<body>
    <?php require_once("../include/views.php"); ?>
    <h1 class="fadeInDown" id="welcome">Website Statistics</h1>

    <section class="fadeInUp">
        <div class="fadeInUp container">
            <div class="fadeInUp row outerDiv">
                <div class="borderDiv fadeInUp col-sm">
                    <h2>Total Views</h2>
                    <?php
                    $query = $db->query("SELECT views FROM views");

                    $totalViews = array();
                    foreach ($query as $row) {
                        array_push($totalViews, $row['views']);
                    }
                    echo "<p style=\"text-align: center\";>";
                    echo array_sum($totalViews);
                    echo "</p>";

                    ?>
                </div>

                <div class="borderDiv fadeInUp col-sm">
                    <h2>Total Viewers</h2>
                    <?php
                    $query = $db->query("SELECT COUNT(*) AS total FROM views");

                    foreach ($query as $count) {
                        echo "<p style=\"text-align: center\";>";
                        echo $count['total'];
                        echo "</p>";
                    }
                    ?>
                </div>
                <div class="borderDiv fadeInUp col-sm">
                    <h2>Your Views</h2>
                    <?php
                    $query = $db->query("SELECT views FROM views WHERE ip=\"" . $_SERVER['REMOTE_ADDR'] . "\"");

                    foreach ($query as $view) {
                        echo "<p style=\"text-align: center\";>";
                        echo $view['views'];
                        echo "</p>";
                    }
                    ?>
                </div>
                <div class="borderDiv fadeInUp col-sm">
                    <h2>Average Views</h2>
                    <?php
                    $query = $db->query("SELECT AVG(views) FROM views");

                    foreach ($query as $row) {
                        echo $row['AVG(views)'];
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="buttonReset">
            <form action="<?php $_SERVER['PHP_SELF'] ?>" method="POST">
                <button type="submit" class="btn btn-danger" id="resetButton" name="reset">Reset my stats</button>
            </form>
        </div>
    </section>
    <?php

    //If reset button gets clicked
    if (isset($_POST['reset'])) {

        //Searching your entry...
        $query = $db->query("SELECT * FROM views WHERE ip=\"" . $_SERVER['REMOTE_ADDR'] . "\"");

        //Feedback if the attempt was succcessfull
        foreach ($query as $row) {
            if (!$db->query("UPDATE views SET views=0 WHERE id=" . $row['id'])) {
                echo "<script>alert(\"Failed to reset stats!\");</script>";
            } else {
                header("Refresh:0");
            }
        }
    }
    ?>
    <script src="stats.js"></script>
</body>

</html>