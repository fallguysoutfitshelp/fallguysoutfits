<?php
/*
Mit dem Datenbank-Server verbinden, Datenbank wählen und Zeichensatz definieren
- mit Fehlerbehandlung

01.09.2020, Tobias Locher
*/

// Verbindung zur Datenbank aufbauen
try {
    $dsn = 'mysql:host=' . $host . ';dbname=' . $database;
    $db = new PDO($dsn, $user, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
}
// Fehler-Behandlung
catch (PDOException $e) {
    // Fehlermeldung ohne Details, wird auch im produktiven Web gezeigt
    echo '<p>Connection Lost!';

    // Detaillierte Fehlermeldung, wird nur auf dem Testserver angezeigt (da, wo display_errors auf on gesetzt ist)
    if (ini_get('display_errors')) {
        echo '<br>' . $e->getMessage();
    }

    // Ausführung des Scripts beenden
    exit;
}
