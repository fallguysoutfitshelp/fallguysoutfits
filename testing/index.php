<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
    <link rel="shortcut icon" href="assets/images/logo-121x126.png" type="image/x-icon">
    <meta name="description" content="Full Fall Guys skin list, daily Fall Guys Shop and an overview of every Fall Guys Skin on one website">
    <meta name="keywords" content="Fall Guys Skins, Fall, Guys, Skins, Outfits, Fallguys, Daily Shop, Fall Guys Daily Shop, Fall Guys Shop, Upper, Lower, Pattern, daily shop">
    <meta name="author" content="Giotsche, Tobi">


    <title>Fall Guys Skins - Daily Shop [FGO]</title>
    <div class="fadeInRight">
        <?php
        require_once("blocks/stylesheets.html");
        ?>
    </div>

</head>

<body>
    <?php
    require_once("include/views.php");
    require_once("include/db_inc.php");
    require_once("include/connect.php");

    $queryUpper = "SELECT * FROM $tableUpper WHERE inShop = 1";
    $queryLower = "SELECT * FROM $tableLower WHERE inShop = 1";
    $queryPattern = "SELECT * FROM $tablePattern WHERE inShop = 1";
    $queryColor = "SELECT * FROM $tableColor WHERE inShop = 1";
    $queryShopextras = "SELECT * FROM $tableShopextras WHERE inShop = 1";
    $queryEmotes = "SELECT * FROM emotes $tableEmotes WHERE inShop = 1";
    ?>
    <section class="menu cid-s8Ra0fAvKt" once="menu" id="menu1-w">

        <?php require_once("blocks/nav.php"); ?>

    </section>

    <section class="features2 cid-s8Ra0hEIR2" id="features3-x">

        <div class="fadeInDown">
            <?php require_once("blocks/alert.php"); ?>
        </div>

        <div class="container fadeInUp">
            <div class="mbr-section-head">
                <div class="card">
                    <div class="card-header container-fluid">
                        <div class="row">
                            <div class="col-sm shopTag">
                                <h1 class="p-3">Shop</h1>
                            </div>
                            <div class="col-sm countdownPC" style="text-align: right;">
                                <h1 class="p-3 countdown"></h1>
                            </div>
                        </div>
                    </div>
                    <ul class="list-group list-group-flush countdownPH">
                        <li class="list-group-item" style="text-align: center;">
                            <h1 class="p-3 countdown"></h1>
                        </li>
                    </ul>

                    <div class="row mt-4">
                        <?php foreach ($db->query($queryShopextras) as $row) { ?>
                            <?php require("blocks/skinFields.php"); ?>
                        <?php } ?>
                        <?php foreach ($db->query($queryPattern) as $row) { ?>
                            <?php require("blocks/skinFields.php"); ?>
                        <?php } ?>
                        <?php foreach ($db->query($queryColor) as $row) { ?>
                            <?php require("blocks/skinFields.php"); ?>
                        <?php } ?>
                        <?php foreach ($db->query($queryUpper) as $row) { ?>
                            <?php require("blocks/skinFields.php"); ?>
                        <?php } ?>
                        <?php foreach ($db->query($queryLower) as $row) { ?>
                            <?php require("blocks/skinFields.php"); ?>
                        <?php } ?>
                        <?php foreach ($db->query($queryEmotes) as $row) { ?>
                            <?php require("blocks/skinFields.php"); ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php
    require_once("blocks/footer.php");
    ?>



    <script>
        // Set the date we're counting down to
        var ownDate = new Date("Oct 03, 2020 11:00:00").getTime();

        // Update the count down every 1 second
        var x = setInterval(function() {

            // Get today's date and time
            var now = new Date().getTime();

            // Find the distance between now and the count down date
            var distance = ownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Display the result in the element with id="countdown"
            document.getElementsByClassName("countdown")[0].innerHTML = days + "d " + hours + "h " +
                minutes + "m " + seconds + "s ";

            //2nd Cowntdown for mobile view
            document.getElementsByClassName("countdown")[1].innerHTML = days + "d " + hours + "h " +
                minutes + "m " + seconds + "s ";

            // If the count down is finished, write some text
            if (distance < 0) {
                clearInterval(x);
                document.getElementsByClassName("countdown")[0].innerHTML = "EXPIRED";

                //2nd expired for mobile view
                document.getElementsByClassName("countdown")[1].innerHTML = "EXPIRED";
            }
        }, 1000);
    </script>

    <!-- include scripts -->
    <?php
    require_once("blocks/javascript.html");
    ?>


</body>

</html>