-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Erstellungszeit: 23. Nov 2020 um 19:38
-- Server-Version: 5.7.32-0ubuntu0.18.04.1
-- PHP-Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `outfits`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` text COLLATE utf8_bin NOT NULL,
  `password` text COLLATE utf8_bin NOT NULL,
  `name` text COLLATE utf8_bin NOT NULL,
  `lastname` text COLLATE utf8_bin NOT NULL,
  `rights` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Daten für Tabelle `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `name`, `lastname`, `rights`) VALUES
(1, 'tobi1879', 'bitch', 'Tobias', 'Locher', 2),
(2, 'gi.oel', 'Costa', 'Gioele', 'Petrillo', 2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `color`
--

CREATE TABLE `color` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sold` int(11) NOT NULL,
  `battlePassLvl` int(11) NOT NULL,
  `dlc` varchar(255) NOT NULL,
  `steamOnly` tinyint(1) NOT NULL,
  `psnOnly` tinyint(1) NOT NULL,
  `price` int(11) NOT NULL,
  `currency` int(11) NOT NULL,
  `rarity` int(11) NOT NULL,
  `inShop` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `season` int(11) NOT NULL,
  `isVid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `color`
--

INSERT INTO `color` (`id`, `name`, `sold`, `battlePassLvl`, `dlc`, `steamOnly`, `psnOnly`, `price`, `currency`, `rarity`, `inShop`, `path`, `season`, `isVid`) VALUES
(1, 'Big Bad', 1, 0, '', 0, 0, 1700, 1, 3, 0, 'bigbadcolor.png', 1, 0),
(2, 'Cherry Cream', 1, 0, '', 0, 0, 1700, 1, 3, 0, 'cherrycreamcolor.png', 1, 0),
(3, 'Valentine', 1, 0, '', 0, 0, 2500, 1, 4, 0, 'valentinecolor.png', 1, 0),
(4, 'Paragon', 1, 0, '', 0, 0, 2, 2, 5, 0, 'paragoncolor.png', 1, 0),
(5, 'Enchanted', 1, 0, '', 0, 0, 2, 2, 5, 0, 'enchantedcolor.png', 2, 0),
(6, 'Pumpkin', 1, 0, '', 0, 0, 2500, 1, 4, 0, 'pumpkincolor.png', 2, 0),
(7, 'Arcane', 1, 0, '', 0, 0, 1700, 1, 3, 1, 'arcanecolor.png', 2, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `emotes`
--

CREATE TABLE `emotes` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `sold` int(11) NOT NULL,
  `battlePassLvl` int(11) NOT NULL,
  `dlc` varchar(255) COLLATE utf8_bin NOT NULL,
  `steamOnly` tinyint(1) NOT NULL,
  `psnOnly` tinyint(1) NOT NULL,
  `price` int(11) NOT NULL,
  `currency` int(11) NOT NULL,
  `rarity` int(11) NOT NULL,
  `inShop` int(11) NOT NULL,
  `path` varchar(255) COLLATE utf8_bin NOT NULL,
  `season` int(11) NOT NULL,
  `isVid` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Daten für Tabelle `emotes`
--

INSERT INTO `emotes` (`id`, `name`, `sold`, `battlePassLvl`, `dlc`, `steamOnly`, `psnOnly`, `price`, `currency`, `rarity`, `inShop`, `path`, `season`, `isVid`) VALUES
(1, 'Slow Clap', 1, 0, '', 0, 0, 3, 2, 4, 0, 'slowclapemote.gif', 1, 1),
(3, 'Z-Snap', 1, 0, '', 0, 0, 4, 1, 4, 0, 'zsnapemote.gif', 1, 1),
(4, 'Come On', 1, 0, '', 0, 0, 3, 2, 4, 0, 'comeonemote.gif', 2, 1),
(5, 'Jester', 1, 0, '', 0, 0, 10000, 1, 4, 0, 'jesteremote.gif', 2, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `lower`
--

CREATE TABLE `lower` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sold` int(11) NOT NULL,
  `battlePassLvl` int(11) NOT NULL,
  `dlc` varchar(255) NOT NULL,
  `steamOnly` tinyint(1) NOT NULL,
  `psnOnly` tinyint(1) NOT NULL,
  `price` int(11) NOT NULL,
  `currency` int(11) NOT NULL,
  `rarity` int(11) NOT NULL,
  `inShop` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `pathLogo` text NOT NULL,
  `season` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `lower`
--

INSERT INTO `lower` (`id`, `name`, `sold`, `battlePassLvl`, `dlc`, `steamOnly`, `psnOnly`, `price`, `currency`, `rarity`, `inShop`, `path`, `pathLogo`, `season`) VALUES
(1, 'Alyx', 1, 0, '', 0, 0, 5, 2, 5, 0, 'alyxlower.png', '', 1),
(2, 'Astronaut', 3, 0, 'Purchase Collector Edition or DLC', 0, 0, 0, 0, 2, 0, 'astronautlower.png', '', 1),
(6, 'Banana Water', 1, 0, '', 0, 0, 4500, 1, 3, 0, 'bananawaterlower.png', '', 1),
(8, 'Big Bad', 4, 0, 'Week 1 exclusive bonus', 0, 0, 0, 0, 3, 0, 'bigbadlower.png', '', 1),
(9, 'Blue Freeze', 3, 0, 'Purchase DLC: Fast Food Costume Pack', 0, 0, 0, 0, 4, 0, 'bluefreezelower.png', '', 1),
(10, 'Bulletkin', 1, 0, '', 0, 0, 5, 2, 5, 0, 'bulletkinlower.png', '', 1),
(11, 'Champ', 1, 0, '', 0, 0, 3, 2, 4, 0, 'champlower.png', '', 1),
(12, 'Chicken', 2, 25, '', 0, 0, 0, 0, 3, 0, 'chickenlower.png', '', 1),
(13, 'Chilly', 1, 0, '', 0, 0, 3, 2, 4, 0, 'chillylower.png', '', 1),
(14, 'Crash Tester', 3, 0, 'Bonus of beta test', 0, 0, 0, 0, 5, 0, 'crashtesterlower.png', '', 1),
(15, 'Ecto Pirate', 3, 0, 'Purchase Collector Edition or DLC: Collector\'s Edition', 0, 0, 0, 0, 4, 0, 'ectopiratelower.png', '', 1),
(16, 'Fairycorn', 3, 0, 'Purchase Collector Edition or DLC: Collector\'s Edition', 0, 0, 0, 0, 4, 0, 'fairycornlower.png', '', 1),
(17, 'Flower Pot', 1, 0, '', 0, 0, 1, 2, 3, 0, 'flowerpotlower.png', '', 1),
(18, 'French Fries', 3, 0, 'Purchase DLC: Fast Food Costume Pack', 0, 0, 0, 0, 4, 0, 'frenchfrieslower.png', '', 1),
(19, 'Golden Hatchling', 1, 0, '', 0, 0, 3, 2, 4, 0, 'goldenhatchlinglower.png', '', 1),
(20, 'Hero', 1, 0, '', 0, 0, 3, 2, 4, 0, 'herolower.png', '', 1),
(21, 'Horsey', 1, 0, '', 0, 0, 1, 2, 3, 0, 'horseylower.png', '', 1),
(22, 'Hot Dog', 2, 22, '', 0, 0, 0, 0, 3, 0, 'hotdoglower.png', '', 1),
(23, 'Hunter', 2, 33, '', 0, 0, 0, 0, 4, 0, 'hunterlower.png', '', 1),
(24, 'Jacket', 1, 0, '', 0, 0, 5, 2, 5, 0, 'jacketlower.png', '', 1),
(25, 'Knockout', 1, 0, '', 0, 0, 2000, 1, 2, 0, 'knockoutlower.png', '', 1),
(26, 'Little Leaguer', 1, 0, '', 0, 0, 3, 2, 4, 0, 'littleleaguerlower.png', '', 1),
(27, 'Mallard', 1, 0, '', 0, 0, 3500, 1, 2, 0, 'mallardlower.png\r\n', '', 1),
(28, 'Master Ninja', 1, 0, '', 0, 0, 5, 2, 5, 0, 'masterninjalower.png', '', 1),
(29, 'Monkey', 1, 0, '', 0, 0, 1, 2, 3, 0, 'monkeylower.png', '', 1),
(30, 'Ninja', 1, 0, '', 0, 0, 7000, 1, 4, 0, 'ninjalower.png', '', 1),
(31, 'P-Body', 1, 0, '', 0, 0, 5, 2, 5, 0, 'pbodylower.png', '', 1),
(32, 'Parrot', 1, 0, '', 0, 0, 4500, 1, 3, 0, 'parrotlower.png', '', 1),
(33, 'Pigeon', 2, 6, '', 0, 0, 0, 0, 1, 0, 'pigeonlower.png', '', 1),
(34, 'Pineapple', 1, 0, '', 0, 0, 2000, 1, 1, 0, 'pineapplelower.png', '', 1),
(35, 'Pitcher Perfect', 1, 0, '', 0, 0, 2000, 1, 1, 0, 'pitcherperfectlower.png', '', 1),
(36, 'Prickles', 4, 0, 'Free giveaway for playing in the first week', 0, 0, 0, 0, 5, 0, 'prickleslower.png', '', 1),
(37, 'Rainbow Water', 1, 0, '', 0, 0, 4500, 1, 3, 0, 'rainbowwaterlower.png', '', 1),
(38, 'Raptor', 1, 0, '', 0, 0, 3500, 1, 1, 0, 'raptorlower.png', '', 1),
(39, 'Rookie', 2, 13, '', 0, 0, 0, 0, 2, 0, 'rookielower.png', '', 1),
(40, 'T-Rex', 1, 0, '', 0, 0, 4500, 2, 3, 0, 'trexlower.png', '', 1),
(41, 'Tasty Burger', 3, 0, 'Purchase DLC: Fast Food Costume Pack', 0, 0, 0, 0, 4, 0, 'tastyburgerlower.png', '', 1),
(42, 'Tomato', 1, 0, '', 0, 0, 1, 2, 3, 0, 'tomatolower.png', '', 1),
(43, 'Topsy', 1, 0, '', 0, 0, 3500, 1, 2, 0, 'topsylower.png', '', 1),
(44, 'Toucan', 1, 0, '', 0, 0, 3, 2, 4, 0, 'toucanlower.png', '', 1),
(45, 'Tropics Toucan', 1, 0, '', 0, 0, 3500, 1, 2, 0, 'tropicstoucanlower.png', '', 1),
(46, 'Turvy', 1, 0, '', 0, 0, 4500, 2, 3, 0, 'turvylower.png', '', 1),
(47, 'Twit', 1, 0, '', 0, 0, 4500, 1, 3, 0, 'twitlower.png', '', 1),
(48, 'Twoo', 1, 0, '', 0, 0, 3, 2, 4, 0, 'twoolower.png', '', 1),
(49, 'White Dove', 1, 0, '', 0, 0, 2000, 1, 1, 0, 'whitedovelower.png', '', 1),
(50, 'Chell', 1, 0, '', 0, 0, 5, 2, 5, 0, 'chelllower.png', '', 1),
(51, 'Scout', 1, 0, '', 1, 0, 5, 2, 5, 0, 'scoutlower.png', '', 1),
(52, 'Fishtank', 1, 0, '', 0, 0, 3, 2, 4, 0, 'fishtanklower.png', '', 1),
(53, 'Gordon Freeman', 4, 0, 'Pre-Order Bonus', 1, 0, 0, 0, 5, 0, 'gordonfreemanlower.png', '', 1),
(54, 'My Friend Pedro', 1, 0, '', 0, 0, 5, 2, 5, 0, 'myfriendpedrolower.png', '', 1),
(55, 'Hatchling', 1, 0, '', 0, 0, 4500, 1, 3, 0, 'hatchlinglower.png', '', 1),
(56, 'Gato Roboto', 1, 0, '', 0, 0, 5, 2, 5, 0, 'gatorobotolower.png', '', 1),
(57, 'Deep Diver', 1, 0, '', 0, 0, 5, 2, 5, 0, 'deepdiverlower.png', '', 1),
(58, 'Gris', 1, 0, '', 0, 0, 5, 2, 5, 0, 'grislower.png', '', 1),
(59, 'Quackers', 1, 0, '', 0, 0, 4500, 1, 3, 0, 'quackerslower.png', '', 1),
(60, 'Scout', 1, 0, '', 0, 0, 5, 2, 5, 0, 'scoutlower.png', '', 1),
(61, 'Duchess', 1, 0, '', 0, 0, 11000, 1, 5, 0, 'duchessupper.png', '', 2),
(62, 'Duchess', 1, 0, '', 0, 0, 11000, 1, 5, 0, 'duchesslower.png', '', 2),
(63, 'Oliver', 1, 0, '', 0, 0, 4500, 1, 3, 0, 'oliverlower.png', '', 2),
(64, 'Sonic The Hedgehog', 1, 0, '', 0, 0, 5, 2, 5, 0, 'sonicthehedgehoglower.png', '', 2),
(65, 'Messenger', 1, 0, '', 0, 0, 5, 2, 5, 0, 'messengerlower.png', '', 2),
(66, 'Princess', 1, 0, '', 0, 0, 7000, 1, 4, 0, 'princesslower.png', '', 2),
(67, 'Wicked Witch', 1, 0, '', 0, 0, 100, 1, 3, 0, 'wickedwitchlower.png', '', 2),
(68, 'Godzilla', 1, 0, '', 0, 0, 5, 2, 5, 1, 'godzillalower.png', '', 2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `pattern`
--

CREATE TABLE `pattern` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sold` int(11) NOT NULL,
  `battlePassLvl` int(11) NOT NULL,
  `dlc` varchar(255) NOT NULL,
  `steamOnly` tinyint(1) NOT NULL,
  `psnOnly` tinyint(1) NOT NULL,
  `price` int(11) NOT NULL,
  `currency` int(11) NOT NULL,
  `rarity` int(11) NOT NULL,
  `inShop` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `season` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `pattern`
--

INSERT INTO `pattern` (`id`, `name`, `sold`, `battlePassLvl`, `dlc`, `steamOnly`, `psnOnly`, `price`, `currency`, `rarity`, `inShop`, `path`, `season`) VALUES
(1, 'Alphabet', 1, 0, '', 0, 0, 2, 2, 4, 0, 'alphabetpattern.png', 1),
(2, 'Banana Water', 1, 0, '', 0, 0, 2, 2, 4, 0, 'bananawaterpattern.png', 1),
(3, 'Big Bad', 1, 0, '', 0, 0, 3500, 1, 3, 0, 'bigbadpattern.png', 1),
(4, 'Cells', 2, 4, '', 0, 0, 0, 0, 1, 0, 'cellspattern.png', 1),
(5, 'Chevrons', 1, 0, '', 0, 0, 3000, 1, 2, 0, 'chevronspattern.png', 1),
(6, 'Chicken', 1, 0, '', 0, 0, 3500, 2, 3, 0, 'chickenpattern.png', 1),
(7, 'Constellations', 1, 0, '', 0, 0, 2, 2, 4, 0, 'constellationspattern.png', 1),
(8, 'Cracked', 1, 0, '', 0, 0, 3000, 1, 2, 0, 'crackedpattern.png', 1),
(9, 'Dots', 1, 0, '', 0, 0, 1800, 1, 1, 0, 'dotspattern.png', 1),
(10, 'Egg', 1, 0, '', 0, 0, 3000, 1, 2, 0, 'eggpattern.png', 1),
(11, 'Face Circle', 1, 0, '', 0, 0, 3000, 1, 2, 0, 'facecirclepattern.png', 1),
(12, 'Fish', 1, 0, '', 0, 0, 3, 2, 5, 0, 'fishpattern.png', 1),
(13, 'Flowers', 1, 0, '', 0, 0, 2, 2, 4, 0, 'flowerspattern.png', 1),
(14, 'Full Body', 4, 0, 'Initially owns', 0, 0, 0, 0, 1, 0, 'fullbodypattern.png', 1),
(15, 'Glorious', 1, 0, '', 0, 0, 3, 2, 5, 0, 'gloriouspattern.png', 1),
(16, 'Gradient', 1, 0, '', 0, 0, 3000, 1, 2, 0, 'gradientpattern.png', 1),
(17, 'Gradient Dots', 1, 0, '', 0, 0, 2, 2, 2, 0, 'gradientdotspattern.png', 1),
(18, 'Hearts', 1, 0, '', 0, 0, 2, 2, 4, 0, 'heartspattern.png', 1),
(19, 'Hoodie', 1, 0, '', 0, 0, 2, 2, 4, 0, 'hoodiepattern.png', 1),
(20, 'Hunter', 2, 31, '', 0, 0, 0, 0, 1, 0, 'hunterpattern.png', 1),
(21, 'Ice Cream', 4, 0, 'Initially owns', 0, 0, 0, 0, 1, 0, 'icecreampattern.png', 1),
(22, 'Jurassica', 1, 0, '', 0, 0, 3500, 1, 3, 0, 'jurassicapattern.png', 1),
(23, 'Lightning', 4, 0, 'Initially owns', 0, 0, 0, 0, 1, 0, 'lightningpattern.png', 1),
(24, 'Losanger', 1, 0, '', 0, 0, 3000, 1, 2, 0, 'losangerpattern.png', 1),
(25, 'Lovely', 1, 0, '', 0, 0, 1800, 1, 1, 0, 'lovelypattern.png', 1),
(26, 'Mountains', 1, 0, '', 0, 0, 3, 2, 5, 0, 'mountainspattern.png', 1),
(27, 'Musical', 1, 0, '', 0, 0, 3500, 1, 3, 0, 'musicalpattern.png', 1),
(28, 'Paint Dipped', 2, 26, '', 0, 0, 0, 0, 4, 0, 'paintdippedpattern.png', 1),
(29, 'Pants', 1, 0, '', 0, 0, 1800, 1, 1, 0, 'pantspattern.png', 1),
(30, 'Pirate', 2, 39, '', 0, 0, 0, 0, 3, 0, 'piratepattern.png', 1),
(31, 'Polkadots', 1, 0, '', 0, 0, 3000, 1, 2, 0, 'polkadotspattern.png', 1),
(32, 'Puzzle', 1, 0, '', 0, 0, 2, 2, 4, 0, 'puzzlepattern.png', 1),
(33, 'Racing Car', 1, 0, '', 0, 0, 1800, 1, 1, 0, 'racingcarpattern.png', 1),
(34, 'Random Checker', 1, 0, '', 0, 0, 3500, 1, 3, 0, 'randomcheckerpattern.png', 1),
(35, 'Spider Webs', 1, 0, '', 0, 0, 5000, 1, 4, 0, 'spiderwebspattern.png', 1),
(36, 'Spooky Doodles', 1, 0, '', 0, 0, 2, 2, 4, 0, 'spookydoodlespattern.png', 1),
(37, 'Sprinkles', 1, 0, '', 0, 0, 3000, 1, 2, 0, 'sprinklespattern.png', 1),
(38, 'Square Camo', 1, 0, '', 0, 0, 3500, 1, 3, 0, 'squarecamopattern.png', 1),
(39, 'Squiggly Camo', 2, 8, '', 0, 0, 0, 0, 2, 0, 'squigglycamopattern.png', 1),
(40, 'Stars', 1, 0, '', 0, 0, 2, 2, 4, 0, 'starspattern.png', 1),
(41, 'Stitches', 1, 0, '', 0, 0, 3000, 1, 2, 0, 'stitchespattern.png', 1),
(42, 'Stripes', 1, 0, '', 0, 0, 3000, 1, 2, 0, 'stripespattern.png', 1),
(43, 'Sunburn', 1, 0, '', 0, 0, 1800, 1, 1, 0, 'sunburnpattern', 1),
(44, 'T-Shirt', 1, 0, '', 0, 0, 1800, 1, 1, 0, 'tshirtpattern.png', 1),
(45, 'Tartan', 1, 0, '', 0, 0, 2, 2, 4, 0, 'tartanpattern.png', 1),
(46, 'Toothpaste', 1, 0, '', 0, 0, 3500, 1, 3, 0, 'toothpastepattern.png', 1),
(47, 'Topology', 2, 12, '', 0, 0, 0, 0, 3, 0, 'topologypattern.png', 1),
(48, 'Topsy', 1, 0, '', 0, 0, 3500, 1, 3, 0, 'topsypattern.png', 1),
(49, 'Undies', 4, 0, 'Initially owns', 0, 0, 0, 0, 1, 0, 'undiespattern.png', 1),
(50, 'Watermelon', 4, 0, 'Initially owns', 0, 0, 0, 0, 2, 0, 'watermelonpattern.png', 1),
(51, 'Wave Gradient', 1, 0, '', 0, 0, 2, 2, 2, 0, 'wavegradientpattern.png', 1),
(52, 'Waves', 1, 0, '', 0, 0, 2, 2, 4, 0, 'wavespattern.png', 1),
(53, 'Zebra Stripes', 2, 37, '', 0, 0, 0, 0, 3, 0, 'zebrastripespattern.png', 1),
(54, 'Harlequin', 1, 0, '', 0, 0, 3500, 1, 3, 0, 'harlequinpattern.png', 1),
(55, 'Skelly', 1, 0, '', 0, 0, 5000, 1, 4, 0, 'skellypattern.png', 1),
(56, 'Hotrod Flames', 1, 0, '', 0, 0, 5000, 1, 4, 0, 'hotrodflamespattern.png', 1),
(57, 'Slush', 1, 0, '', 0, 0, 5000, 1, 4, 0, 'slushpattern.png', 1),
(58, 'Diamonds', 1, 0, '', 0, 0, 2, 2, 4, 0, 'diamondspattern.png', 1),
(59, 'T-Rex', 1, 0, '', 0, 0, 3500, 1, 3, 0, 'trexpattern.png', 1),
(60, 'Chainmail', 1, 0, '', 0, 0, 3, 2, 2, 0, 'chainmailpattern.png', 2),
(61, 'Stitch-Up', 1, 0, '', 0, 0, 3500, 1, 3, 0, 'stitch-uppattern.png', 2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shopextras`
--

CREATE TABLE `shopextras` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sold` int(11) NOT NULL,
  `battlePassLvl` int(11) NOT NULL,
  `dlc` varchar(255) NOT NULL,
  `steamOnly` tinyint(1) NOT NULL,
  `psnOnly` tinyint(1) NOT NULL,
  `price` int(11) NOT NULL,
  `currency` int(11) NOT NULL,
  `rarity` int(11) NOT NULL,
  `inShop` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `season` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `shopextras`
--

INSERT INTO `shopextras` (`id`, `name`, `sold`, `battlePassLvl`, `dlc`, `steamOnly`, `psnOnly`, `price`, `currency`, `rarity`, `inShop`, `path`, `season`) VALUES
(50, 'Knock off', 1, 0, '', 0, 0, 8000, 1, 3, 0, 'knockoffshopextras.png', 1),
(51, 'Inverted', 1, 0, '', 0, 0, 2, 2, 5, 0, 'invertedshopextras.png', 1),
(52, 'Rosy Cheeks', 1, 0, '', 0, 0, 3000, 1, 3, 0, 'rosycheeksshopextras.png', 1),
(54, 'Soon available!', 1, 0, '', 0, 0, 0, 0, 0, 0, 'soonavailable.png', 1),
(55, 'Parkour!', 1, 0, '', 0, 0, 1700, 1, 3, 0, 'parkournickname.png', 2),
(56, 'Shock', 1, 0, '', 0, 0, 3000, 1, 3, 0, 'shockshopextras.png', 2),
(57, 'Free Hugs', 1, 0, '', 0, 0, 1, 2, 4, 0, 'freehugsshopextras.png', 2),
(58, 'Pumpkin', 1, 0, '', 0, 0, 1, 2, 4, 0, 'pumpkinshopextras.png', 2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shopRating`
--

CREATE TABLE `shopRating` (
  `id` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `ip` varchar(15) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `upper`
--

CREATE TABLE `upper` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sold` int(11) NOT NULL,
  `battlePassLvl` int(11) NOT NULL,
  `dlc` varchar(255) NOT NULL,
  `steamOnly` tinyint(1) NOT NULL,
  `psnOnly` tinyint(1) NOT NULL,
  `price` int(11) NOT NULL,
  `currency` int(11) NOT NULL,
  `rarity` int(11) NOT NULL,
  `inShop` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `season` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `upper`
--

INSERT INTO `upper` (`id`, `name`, `sold`, `battlePassLvl`, `dlc`, `steamOnly`, `psnOnly`, `price`, `currency`, `rarity`, `inShop`, `path`, `season`) VALUES
(1, 'Alyx', 1, 0, '', 0, 0, 5, 2, 5, 0, 'alyxupper.png', 1),
(2, 'Astronaut', 3, 0, 'Purchase Collector Edition or DLC', 0, 0, 0, 0, 2, 0, 'astronautupper.png', 1),
(6, 'Banana Water', 1, 0, '', 0, 0, 4500, 1, 3, 0, 'bananawaterupper.png', 1),
(8, 'Big Bad', 4, 0, 'Week 1 exclusive bonus', 0, 0, 0, 0, 3, 0, 'bigbadupper.png', 1),
(9, 'Blue Freeze', 3, 0, 'Purchase DLC: Fast Food Costume Pack', 0, 0, 0, 0, 4, 0, 'bluefreezeupper.png', 1),
(10, 'Bulletkin', 1, 0, '', 0, 0, 5, 2, 5, 0, 'bulletkinupper.png', 1),
(11, 'Champ', 1, 0, '', 0, 0, 3, 2, 4, 0, 'champupper.png', 1),
(12, 'Chicken', 2, 28, '', 0, 0, 0, 0, 3, 0, 'chickenupper.png', 1),
(13, 'Chilly', 1, 0, '', 0, 0, 3, 2, 4, 0, 'chillyupper.png', 1),
(14, 'Crash Tester', 3, 0, 'Bonus of beta test', 0, 0, 0, 0, 5, 0, 'crashtesterupper.png', 1),
(15, 'Ecto Pirate', 3, 0, 'Purchase Collector Edition or DLC: Collector\'s Edition', 0, 0, 0, 0, 4, 0, 'ectopirateupper.png', 1),
(16, 'Fairycorn', 3, 0, 'Purchase Collector Edition or DLC: Collector\'s Edition', 0, 0, 0, 0, 4, 0, 'fairycornupper.png', 1),
(17, 'Flower Pot', 1, 0, '', 0, 0, 1, 2, 3, 0, 'flowerpotupper.png', 1),
(18, 'French Fries', 3, 0, 'Purchase DLC: Fast Food Costume Pack', 0, 0, 0, 0, 4, 0, 'frenchfriesupper.png', 1),
(19, 'Golden Hatchling', 1, 0, '', 0, 0, 3, 2, 4, 0, 'goldenhatchlingupper.png', 1),
(20, 'Hero', 1, 0, '', 0, 0, 3, 2, 4, 0, 'heroupper.png', 1),
(21, 'Horsey', 1, 0, '', 0, 0, 1, 2, 3, 0, 'horseyupper.png', 1),
(22, 'Hot Dog', 2, 19, '', 0, 0, 0, 0, 3, 0, 'hotdogupper.png', 1),
(23, 'Hunter', 2, 36, '', 0, 0, 0, 0, 4, 0, 'hunterupper.png', 1),
(24, 'Jacket', 1, 0, '', 0, 0, 5, 2, 5, 0, 'jacketupper.png', 1),
(25, 'Knockout', 1, 0, '', 0, 0, 2000, 1, 2, 0, 'knockoutupper.png', 1),
(26, 'Little Leaguer', 1, 0, '', 0, 0, 3, 2, 4, 0, 'littleleaguerupper.png', 1),
(27, 'Mallard', 1, 0, '', 0, 0, 3500, 1, 2, 0, 'mallardupper.png\r\n', 1),
(28, 'Master Ninja', 1, 0, '', 0, 0, 5, 2, 5, 0, 'masterninjaupper.png', 1),
(29, 'Monkey', 1, 0, '', 0, 0, 1, 2, 3, 0, 'monkeyupper.png', 1),
(30, 'Ninja', 1, 0, '', 0, 0, 7000, 1, 4, 0, 'ninjaupper.png', 1),
(31, 'P-Body', 1, 0, '', 0, 0, 5, 2, 5, 0, 'pbodyupper.png', 1),
(32, 'Parrot', 1, 0, '', 0, 0, 4500, 1, 3, 0, 'parrotupper.png', 1),
(33, 'Pigeon', 2, 9, '', 0, 0, 0, 0, 1, 0, 'pigeonupper.png', 1),
(34, 'Pineapple', 1, 0, '', 0, 0, 2000, 1, 1, 0, 'pineappleupper.png', 1),
(35, 'Pitcher Perfect', 1, 0, '', 0, 0, 2000, 1, 1, 0, 'pitcherperfectupper.png', 1),
(36, 'Prickles', 4, 0, 'Free giveaway for playing in the first week', 0, 0, 0, 0, 5, 0, 'pricklesupper.png', 1),
(37, 'Rainbow Water', 1, 0, '', 0, 0, 4500, 1, 3, 0, 'rainbowwaterupper.png', 1),
(38, 'Raptor', 1, 0, '', 0, 0, 3500, 1, 1, 0, 'raptorupper.png', 1),
(39, 'Rookie', 2, 16, '', 0, 0, 0, 0, 2, 0, 'rookieupper.png', 1),
(40, 'T-Rex', 1, 0, '', 0, 0, 4500, 2, 3, 0, 'trexupper.png', 1),
(41, 'Tasty Burger', 3, 0, 'Purchase DLC: Fast Food Costume Pack', 0, 0, 0, 0, 4, 0, 'tastyburgerupper.png', 1),
(42, 'Tomato', 1, 0, '', 0, 0, 1, 2, 3, 0, 'tomatoupper.png', 1),
(43, 'Topsy', 1, 0, '', 0, 0, 3500, 1, 2, 0, 'topsyupper.png', 1),
(44, 'Toucan', 1, 0, '', 0, 0, 3, 2, 4, 0, 'toucanupper.png', 1),
(45, 'Tropics Toucan', 1, 0, '', 0, 0, 3500, 1, 2, 0, 'tropicstoucanupper.png', 1),
(46, 'Turvy', 1, 0, '', 0, 0, 4500, 2, 3, 0, 'turvyupper.png', 1),
(47, 'Twit', 1, 0, '', 0, 0, 4500, 1, 3, 0, 'twitupper.png', 1),
(48, 'Twoo', 1, 0, '', 0, 0, 3, 2, 4, 0, 'twooupper.png', 1),
(49, 'White Dove', 1, 0, '', 0, 0, 2000, 1, 1, 0, 'whitedoveupper.png', 1),
(50, 'Chell', 1, 0, '', 0, 0, 5, 2, 5, 0, 'chellupper.png', 1),
(51, 'Scout', 1, 0, '', 1, 0, 5, 2, 5, 0, 'scoutupper.png', 1),
(52, 'Fishtank', 1, 0, '', 0, 0, 3, 2, 4, 0, 'fishtankupper.png', 1),
(53, 'Gordon Freeman', 4, 0, 'Pre-Order Bonus', 1, 0, 0, 0, 5, 0, 'gordonfreemanupper.png', 1),
(54, 'My Friend Pedro', 1, 0, '', 0, 0, 5, 2, 5, 0, 'myfriendpedroupper.png', 1),
(55, 'Hatchling', 1, 0, '', 0, 0, 4500, 1, 3, 0, 'hatchlingupper.png', 1),
(56, 'Gato Roboto', 1, 0, '', 0, 0, 5, 2, 5, 0, 'gatorobotoupper.png', 1),
(57, 'Deep Diver', 1, 0, '', 0, 0, 5, 2, 5, 0, 'deepdiverupper.png', 1),
(58, 'Gris', 1, 0, '', 0, 0, 5, 2, 5, 0, 'grisupper.png', 1),
(59, 'Quackers', 1, 0, '', 0, 0, 4500, 1, 3, 0, 'quackersupper.png', 1),
(60, 'Scout', 1, 0, 'aber ', 0, 0, 5, 2, 5, 0, 'scoutupper.png', 1),
(61, 'Duchess', 1, 0, '', 0, 0, 11000, 1, 5, 0, 'duchessupper.png', 2),
(62, 'Oliver', 1, 0, '', 0, 0, 4500, 1, 3, 0, 'oliverupper.png', 2),
(63, 'Sonic The Hedgehog', 1, 0, '', 0, 0, 5, 2, 5, 0, 'sonicthehedgehogupper.png', 2),
(64, 'Messenger', 1, 0, '', 0, 0, 5, 2, 5, 0, 'messengerupper.png', 2),
(65, 'Princess', 1, 0, '', 0, 0, 7000, 1, 4, 0, 'princessupper.png', 2),
(67, 'Wicked Witch', 1, 0, '', 0, 0, 100, 1, 3, 0, 'wickedwitchupper.png', 2),
(68, 'Godzilla', 1, 0, '', 0, 0, 5, 2, 5, 1, 'godzillaupper.png', 2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `upperSkin`
--

CREATE TABLE `upperSkin` (
  `id` int(11) NOT NULL,
  `sold` tinyint(1) NOT NULL,
  `steamOnly` tinyint(1) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Daten für Tabelle `upperSkin`
--

INSERT INTO `upperSkin` (`id`, `sold`, `steamOnly`, `price`) VALUES
(1, 1, 0, 12),
(2, 0, 1, 23);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `color`
--
ALTER TABLE `color`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `emotes`
--
ALTER TABLE `emotes`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `lower`
--
ALTER TABLE `lower`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `pattern`
--
ALTER TABLE `pattern`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `shopextras`
--
ALTER TABLE `shopextras`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `shopRating`
--
ALTER TABLE `shopRating`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `upper`
--
ALTER TABLE `upper`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `upperSkin`
--
ALTER TABLE `upperSkin`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT für Tabelle `color`
--
ALTER TABLE `color`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT für Tabelle `emotes`
--
ALTER TABLE `emotes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT für Tabelle `lower`
--
ALTER TABLE `lower`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT für Tabelle `pattern`
--
ALTER TABLE `pattern`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT für Tabelle `shopextras`
--
ALTER TABLE `shopextras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT für Tabelle `shopRating`
--
ALTER TABLE `shopRating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `upper`
--
ALTER TABLE `upper`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT für Tabelle `upperSkin`
--
ALTER TABLE `upperSkin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
