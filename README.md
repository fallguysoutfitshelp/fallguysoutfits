# Fallguysoutfits Website

Fallguysoutfits is a website, that lists skins from the popular game: **Fallguys**

## Features

- Searching skins
- Daily shop
- Price list

## Potential features

- Fallguys game update news

## Contributors


| Nick | E-Mail |
| ------ | ------ |
| Tobi | tobi1879@gmail.com |
| Giotsche | gioele.petrillo@gmail.com |